﻿using CodeBase.Data;

namespace CodeBase.Services.PersistensProgress
{
    public class PersistentProgressService : IPersistentProgressService
    {
        public PlayerProgress Progress { get; set; }
    }
}