using CodeBase.Infrastructure.AssetManagement;
using UnityEngine;

namespace CodeBase.Infrastructure.Factory // фабрика для создания обьектов
{
  public class GameFactory : IGameFactory
  {
    private readonly IAssetProvider _assets;

    public GameFactory(IAssetProvider assets)
    {
      _assets = assets;
    }

    public GameObject CreateHero(GameObject prefab)
    {
      return _assets.Instantiate(path: AssetPath.HeroPath, at: prefab.transform.position);
    }

    public void CreateHud() =>
      _assets.Instantiate(AssetPath.HudPath);
  }
}