namespace CodeBase.Infrastructure.Services
{
  public class AllServices // сервис локатор (синглтон) для регистрации сервисов
  {
    private static AllServices _instance;
    public static AllServices Container => _instance ?? (_instance = new AllServices()); // синглтон с ленивой иницилизацией (контейнер сервисов)

    //метод который регестрирует синглтон какой нибудь реализации
    public void RegisterSingle<TService>(TService implementation) where TService : IService =>
      Implementation<TService>.ServiceInstance = implementation;

    // метод Single вернет синглтон той реализации что сюжа попадет по своему типу
    public TService Single<TService>() where TService : IService =>
      Implementation<TService>.ServiceInstance;

    private class Implementation<TService> where TService : IService // при обращении сегнерирует по дженерик параметру свой класс для каждого случая 
    {
      public static TService ServiceInstance;
    }
  }
}