﻿using System;
using System.Collections.Generic;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using CodeBase.Logic;

namespace CodeBase.Infrastructure.States
{
  public class GameStateMachine // стейт машина, тут записываем и создаем все стейты и пользуемся методами стейт машины
  {
    private Dictionary<Type, IExitableState> _states; // все стейты
    private IExitableState _activeState; // акттвный стейт

    //все коснтрукторы в данной архитектуре создают явные зависимости переписывая в поле при создании обьекта
    public GameStateMachine(SceneLoader sceneLoader, LoadingCurtain loadingCurtain, AllServices services) // конструтор для стейт машины
    {
      _states = new Dictionary<Type, IExitableState> // записываем сюда все стейты
      {
        // создаем и записываем все стейт и все что они требуют пробрасываем в конструктор GameStateMachine
        [typeof(BootstrapState)] = new BootstrapState(this, sceneLoader, services),
        [typeof(LoadLevelState)] = new LoadLevelState(this, sceneLoader, loadingCurtain, services.Single<IGameFactory>()),
        [typeof(GameLoopState)] = new GameLoopState(this),
      };
    }
    
    public void Enter<TState>() where TState : class, IState // стейт без ничего, просто для запуска
    {
      IState state = ChangeState<TState>();
      state.Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload> // стейт со стрингом чисто для загрузки сцен
    {
      TState state = ChangeState<TState>();
      state.Enter(payload);
    }

    private TState ChangeState<TState>() where TState : class, IExitableState // смена стейта
    {
      _activeState?.Exit();
      
      TState state = GetState<TState>();
      _activeState = state;
      
      return state;
    }

    private TState GetState<TState>() where TState : class, IExitableState => 
      _states[typeof(TState)] as TState;
  }
}