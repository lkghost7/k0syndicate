﻿using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.Factory;
using CodeBase.Infrastructure.Services;
using CodeBase.Services.Input;
using CodeBase.Services.PersistensProgress;
using CodeBase.StaticData;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
  public class BootstrapState : IState // стейт для стартовой загрузки инитов и тут регаем сервисы
  {
    private const string Initial = "Initial";
    private readonly GameStateMachine _stateMachine;
    private readonly SceneLoader _sceneLoader;
    private readonly AllServices _services; // все сервисы

    public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader, AllServices services) // в конструктор пишем все явные зависимости
    {
      //при старте игры регистрируем все
      _stateMachine = stateMachine;
      _sceneLoader = sceneLoader;
      _services = services;

      RegisterServices(); // регистрируем ссылки на серивисы
    }

    public void Enter() // входим в стейт
    {
      _sceneLoader.Load(Initial, onLoaded: EnterLoadLevel); // грузим лвл
    }

    public void Exit()
    {
    }

    private void RegisterServices() // для регистрации сервисов при загрузке 
    {
      _services.RegisterSingle<IInputService>(InputService()); // регистрируем инпут сервис и даем на вход метод с выбором инпута
      _services.RegisterSingle<IAssetProvider>(new AssetProvider()); // регаем ассет провайдер
      _services.RegisterSingle<IPersistentProgressService>(new PersistentProgressService()); //персистенс сервис
      _services.RegisterSingle<IGameFactory>(new GameFactory(_services.Single<IAssetProvider>()));

      RegisterStaticData();
    }

    private void RegisterStaticData()
    {
      var staticData = new StaticDataService();
      staticData.LoadMonsters();
      _services.RegisterSingle(staticData);
    }

    private void EnterLoadLevel() => _stateMachine.Enter<LoadLevelState, string>("Main");

    private static IInputService InputService()
    {
      if (Application.isEditor)
        return new StandaloneInputService();
      else
        return new MobileInputService();
    }
  }
}