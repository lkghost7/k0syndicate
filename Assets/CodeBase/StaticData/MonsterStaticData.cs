using UnityEngine;

namespace CodeBase.StaticData
{
    [CreateAssetMenu(fileName = "MonterData", menuName = "StaticData/Monster")]
    public class MonsterStaticData : ScriptableObject
    {
        public MonsterTypeId MonsterTypeId;
        [Range(1, 100)]
        public int Hp;
        [Range(1, 30)]
        public float Damage;
 
        [Range(0.5f, 1f)]
        public float EffectiveDistance;
        public float Cleavage;

        public GameObject prefab;
    }
}